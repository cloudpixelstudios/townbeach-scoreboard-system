import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss']
})
export class LiveComponent implements OnInit {

  public loading = true;

  public placeholderList = ["../assets/logos/townbeach_logo.png", "../assets/logos/cloudpixel_logo.png", "../assets/logos/Hot_Food_Now.png"];
  public placeholderImage = 0;

  public screensObservableSocket = this.socket.fromEvent<string[]>('scoreboards');
  private screensSubject = new BehaviorSubject(null);
  public screens = this.screensSubject.asObservable();

  public timer1Text;
  public startTime1;
  public timer2Text;
  public startTime2;
  public timerRef = [undefined, undefined];

  public gameLength = 40 * 60;
  //public gameLength = 20.25 * 60;
  public buzzerIsOn = [false, false];

  public winnersFlash = 0;

  constructor(public _database: AngularFireDatabase, private socket: Socket) {
    let that = this;
    that.screensObservableSocket.subscribe(data => {
      console.log(data)
      if(data[0]["status"] == 'inGame') {
        if(!this.startTime1){
          that.startGameTimer(0)
        }
      }
      if (data[1]["status"] == 'inGame') {
        if(!this.startTime2){
          that.startGameTimer(1)
        }
      }
      if(data[0]["status"] == 'gameOver') {
        clearTimeout(that.timerRef[0]);
        that.startTime1 = undefined;
        console.log("cleared game timer")
      }
      if(data[1]["status"] == 'gameOver') {
        clearTimeout(that.timerRef[1]);
        that.startTime2 = undefined;
        console.log("cleared game timer 2")
      }
      that.screensSubject.next(data);
      that.loading = false;
    })
    that.playImageRotation();
    that.playWinnerRotation();
  }

  playBuzzer(court) {
    this.socket.emit('buzzer', {screen_id: court});
  }

  startGameTimer(i) {
    this.playBuzzer(i);
    this.buzzerIsOn[i] = true;
    if (i == 0) {
      this.startTime1 = Date.now();
      this.gameTimeTick(i);
    } else {
      this.startTime2 = Date.now();
      this.gameTimeTick(i);
    }
  }

  gameTimeTick(i) {
    let that = this;
    let currentRemaining;
    that.timerRef[i] = setTimeout(function() {
      if(i == 0) {
        let delta = Date.now() - that.startTime1;
        let elapsed = Math.floor(delta / 1000);
        currentRemaining = that.gameLength-elapsed;
        that.timer1Text = that.fancyTimeFormat(currentRemaining);
      } else {
        let delta = Date.now() - that.startTime2;
        let elapsed = Math.floor(delta / 1000);
        currentRemaining = that.gameLength-elapsed;
        that.timer2Text = that.fancyTimeFormat(currentRemaining);
      }
      if (currentRemaining == 1200) {
        if(!that.buzzerIsOn[i]) {
          that.playBuzzer(i);
          that.buzzerIsOn[i] = true;
        }
        that.gameTimeTick(i);
      } else if (currentRemaining == 0){
        if(!that.buzzerIsOn[i]) {
          that.playBuzzer(i);
          that.buzzerIsOn[i] = true;
        }
      } else if (currentRemaining == 2280){
        if(!that.buzzerIsOn[i]) {
          that.playBuzzer(i);
          that.buzzerIsOn[i] = true;
        }
        that.gameTimeTick(i);
      } else if (currentRemaining == 1080){
        if(!that.buzzerIsOn[i]) {
          that.playBuzzer(i);
          that.buzzerIsOn[i] = true;
        }
        that.gameTimeTick(i);
      } else {
        that.buzzerIsOn[i] = false;
        that.gameTimeTick(i);
      }
    }, 200);
  }

  fancyTimeFormat(duration){
      // Hours, minutes and seconds
      var hrs = ~~(duration / 3600);
      var mins = ~~((duration % 3600) / 60);
      var secs = ~~duration % 60;

      // Output like "1:01" or "4:03:59" or "123:03:59"
      var ret = "";

      if (hrs > 0) {
          ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
      }

      ret += "" + mins + ":" + (secs < 10 ? "0" : "");
      ret += "" + secs;
      return ret;
  }

  playWinnerRotation() {
    let that = this;
    setTimeout(() => {
      if(that.winnersFlash == 1) {
        that.winnersFlash = 0;
      } else {
        that.winnersFlash += 1;
      }
      that.playWinnerRotation();
    }, 6000);
  }

  playImageRotation() {
    let that = this;
    setTimeout(() => {
      if(that.placeholderImage == that.placeholderList.length - 1) {
        that.placeholderImage = 0;
      } else {
        that.placeholderImage += 1;
      }
      that.playImageRotation();
    }, 5000);
  }

  ngOnInit(): void {
  }

}
